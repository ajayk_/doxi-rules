

README.rulesets for doxi / dogtown-naxi-rules

version: 2013-02-08

these rulesets are now available as independent git-repo @ 
https://bitbucket.org/lazy_dogtown/doxi-rules

for tools to manage your doxi-rules you might want to install doxi-tools
https://bitbucket.org/lazy_dogtown/doxi

to keep track of changes and ruleset-updates you could either 
subscribe to the doxi-updates - blog http://doxi-news.blogspot.de/ ,
subscribe to the naxsi-mailinglist 
https://groups.google.com/forum/?fromgroups#!forum/naxsi-discuss
subscribe to the ruleset-feed https://bitbucket.org/lazy_dogtown/doxi-rules/rss
or follow that project on Bitbucket

License: see License.txt



all not-mentioned files here are part of naxsi/nginx - default-configuration


--[ configuration rules ]------------------------------------------

please note: due to changes in naxsi after 0.49 this file-layout might get 
obsolete. 

rules.conf
    your global includes-file; you might setup different rules.con - files,
    maybe tuned for each virtualhost.


learning-mode.rules 
    rules to configure learning-mode 
    
active-mode.rules 
    rules to configure active-mode 


--[ detection rules ]-----------------------------------------------

app_server.rules
    rules you might want to enable when running nginx as proxy
    for app-servers like tomcat / rails etc and you're shure to
    have no php/asp/cgi - files lying around

malware.rules
    NOTE: for a better coverage you might want to try a real ids
    like snort or suricata  with et-rulesets rules to detect malicious
    content in- and outbound 
    
    this ruleset is designed to detect malicious request that give a 
    hint for hacked / misused / C&C-servers and tries to detect
    web-backdoors, webshells and other malicious access to unwanted
    files/services.
    
    these rules are quite noise, so if included you might want to
    tune and create whitelists for your applications
    
scanner.rules
    
    detect scanners (WebAppScanners/Testing-Tools, but also 
    vuln-scanning-bots or attack-tools) by UA or by certain requests.
    some of these rules could be included into web_[app|server].rules,
    like scanners for certain webapp/server-vulns, but when there's a 
    clear sign for an automated scanning-process the sigs are include here
    sources: own logs, et, mod_sec_core_ruleset
    

web_app.rules
    detect exploit/misuse-attempts againts web-applications; please see 
    scanner.rules for some details on webapp-based scanners

web_server.rules
    
    generic rules to protect a webserver from misconfiguration 
    and known mistakes / exploit-vectors 


--[ misc. rules ]-----------------------------------------------------

misc_whitelisting.rules 
    whitelistings for different webapps/actions that are known to fail
    on certain parameters 
    
